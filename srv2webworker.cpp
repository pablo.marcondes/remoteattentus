#include "srv2webworker.h"
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QFileInfo>

Srv2WebWorker::Srv2WebWorker(QSettings *settings):
    settings(settings),
    httpClient(NULL),
    httpUploadClient(NULL),
    sftpClient(NULL),
    isInitialized(false)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
}

Srv2WebWorker::~Srv2WebWorker()
{
    delete httpClient;
    delete httpUploadClient;
    delete sftpClient;
}

void Srv2WebWorker::initialize()
{    
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;

    if(!isInitialized) {
        isInitialized = true;

        httpClient = new HttpClient();
        connect(httpClient, SIGNAL(dataReady(QByteArray)), this, SLOT(parseJson(QByteArray)));

        httpUploadClient = new HttpClient();
        connect(httpUploadClient, SIGNAL(downloadFinished()), this, SLOT(handleFinishedDownload()));

        sftpClient = new SftpClient();
        connect(sftpClient, SIGNAL(transferFinished()), this, SLOT(handleTransferFinished()));
    }

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(getSrvs2Web()));
    timer->start(30000);

    emit initialized();
}

void Srv2WebWorker::handleFinishedDownload()
{
    qDebug() << "realizou upload";
}

void Srv2WebWorker::handleTransferFinished()
{
    QString uploadUrl = settings->value("RemoteAttentus/download", "http://54.164.73.251/webcmd/static").toString()
            .replace("static", "upload");
    httpUploadClient->upload(uploadUrl, new QFile(QFileInfo(filePath).fileName()));
}

void Srv2WebWorker::parseJson(const QByteArray &data)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(data);
    QJsonObject object = jsonDoc.object();

    if(object.contains("srv2web")) {
        QJsonArray array = object.value("srv2web").toArray();
        foreach (const QJsonValue value, array)
        {
            filePath = value.toString().replace("\\\\","\\");
            qDebug() << "srv2web=" << filePath;
            sftpClient->host(settings->value("RemoteAttentus/host", "172.29.1.158").toString())->
                    user(settings->value("RemoteAttentus/login", "attentus").toString())->
                    pass("sant3f0r,NP@!AT")->
                    port(settings->value("RemoteAttentus/port", "37259").toInt());
            sftpClient->localFile(filePath)->remoteFile(filePath);
            sftpClient->download();
        }
    }
}

void Srv2WebWorker::getSrvs2Web()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QString url = settings->value("RemoteAttentus/cmds", "http://54.164.73.251/webcmd/getcmds").toString()
            .replace("getcmds", "getsrv2web");
    httpClient->downloadFile(url);
}
