#ifndef SSHWORKER_H
#define SSHWORKER_H

#include "sftpchannel.h"
#include "sshconnection.h"
#include "sshremoteprocess.h"

#include <QMutex>
#include <QWaitCondition>
#include <QThread>
#include <QQueue>
#include <QTimer>

class SshWorker : public QObject
{
    Q_OBJECT

public:
    SshWorker(QSsh::SshConnectionParameters &parameters);
    ~SshWorker();

    void sendCommand(QString cmd);
    void startConnection();
    void doDisconnect();
    void disable();
    void setParameters(QSsh::SshConnectionParameters parameters);

public slots:
    void initialize();

private:
    QSsh::SshConnection *m_connection;
    QSsh::SshConnectionParameters parameters;
    QSharedPointer<QSsh::SshRemoteProcess> m_shell;
    QFile * const m_stdin;
    QMutex *queueMutex;
    QWaitCondition hasMessage;
    bool keepRunning;
    QQueue<QString> cmdQueue;
    QTimer *timer;    

private slots:
    void handleConnected();
    void handleShellMessage(const QString str);
    void handleConnectionError(QSsh::SshError error);
    void handleShellStarted();
    void handleRemoteStdout();
    void handleRemoteStderr();
    void handleChannelClosed(int exitStatus);
    void handleStdin();
    void send();

signals:
    void connected();
    void shellOutput(const QByteArray str);
    void connectionError(const QSsh::SshError error);
};

#endif // SSHWORKER_H
