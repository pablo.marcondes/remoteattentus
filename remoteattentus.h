#ifndef REMOTEATTENTUS_H
#define REMOTEATTENTUS_H

#include <QMainWindow>

#include <QNetworkAccessManager>
#include <QFile>
#include <QSettings>
#include <QThread>
#include <iostream>
#include "ui_remoteattentus.h"
#include "cmdworker.h"
#include "web2srvworker.h"
#include "srv2webworker.h"
#include "updateworker.h"

class RemoteAttentus : public QMainWindow
{
    Q_OBJECT

public:
    RemoteAttentus(QWidget *parent = 0);
    ~RemoteAttentus();

private:
    Ui::RemoteAttentus* ui;
    bool flagConnected;

    QSettings *settings;
    QFile *file;
    CmdWorker *cmdWorker;
    Web2SrvWorker *web2SrvWorker;
    Srv2WebWorker *srv2WebWorker;
    UpdateWorker *updateWorker;
    QSsh::SshConnectionParameters parameters;
    QThread cmdWorkerThread;
    QThread web2SrvWorkerThread;
    QThread srv2WebWorkerThread;
    QThread updateWorkerThread;

private slots:
    void connectClicked();
    void disconnectClicked();
    void updateDownloads(bool checked);
    void uiEnableConnected();
    void uiDisableConnected();
    void updateParams();
    void prepareToUpdate();

signals:
    void letStartUpdates();
    void letDownload();

};
#endif // REMOTEATTENTUS_H
