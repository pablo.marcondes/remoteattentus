/**************************************************************************
**
** This file is part of QSsh
**
** Copyright (c) 2012 LVK
**
** Contact: andres.pagliano@lvklabs.com
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
**************************************************************************/

#ifndef SECUREFILEUPLOADER_H
#define SECUREFILEUPLOADER_H

#include <QObject>

#include "sftpchannel.h"
#include "sshconnection.h"

/// Very simple example to upload a file using FTPS
class SftpClient : public QObject
{
    Q_OBJECT
public:
    enum TransferMode {
        DOWNLOAD,
        UPLOAD
    };

    explicit SftpClient(QObject *parent = 0);

    SftpClient* localFile(const QString &localFile);
    SftpClient* remoteFile(const QString &remoteFile);
    SftpClient* host(const QString &host);
    SftpClient* port(const quint16 &port);
    SftpClient* user(const QString &user);
    SftpClient* pass(const QString &pass);
    SftpClient* recursive(const bool &recursive);
    SftpClient* overwrite(const bool &overwrite);
    SftpClient* transferMode(const TransferMode &transferMode);

    bool isConfigured();

    /// Uploads \a localFile to \a username@host:/dest using password \a passwd
    void upload();

    void download();
    QSsh::SshConnectionParameters createParams();

signals:
    void transferFinished();

private slots:
    void onConnected();
    void onConnectionError(QSsh::SshError);
    void onChannelInitialized();
    void onChannelError(const QString &err);
    void onOpfinished(QSsh::SftpJobId job, const QString & error = QString());

private:
    bool m_recursive;
    QSsh::SftpOverwriteMode m_overwriteMode;
    QString m_localFilename;
    QString m_remoteFilename;
    QString m_host;
    QString m_user;
    QString m_pass;
    quint16 m_port;
    TransferMode m_transferMode;
    QSsh::SftpChannel::Ptr m_channel;
    QSsh::SshConnection *m_connection;

    void parseDestination(const QString &dest);
    void connectAndTransfer();

};

#endif // SECUREFILEUPLOADER_H
