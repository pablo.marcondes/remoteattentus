#-------------------------------------------------
#
# Project created by QtCreator 2014-12-31T11:27:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RemoteAttentus
TEMPLATE = app


SOURCES += main.cpp\
        remoteattentus.cpp \
    cmdworker.cpp \
    sshworker.cpp \
    updateworker.cpp \
    sftpclient.cpp \
    httpclient.cpp \
    web2srvworker.cpp \
    srv2webworker.cpp

HEADERS  += remoteattentus.h \
    cmdworker.h \
    sshworker.h \
    updateworker.h \
    sftpclient.h \
    httpclient.h \
    web2srvworker.h \
    srv2webworker.h

FORMS    += remoteattentus.ui

unix:!macx|win32: LIBS += -L$$PWD/../build-qssh-Desktop-Debug/lib/ -lQSsh -lBotan

INCLUDEPATH += $$PWD/../QSsh/src/libs/ssh
DEPENDPATH += $$PWD/../QSsh/src/libs/ssh
