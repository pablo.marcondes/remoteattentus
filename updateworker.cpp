#include "updateworker.h"

#include <QProcess>
#include <QDir>
#include <QCoreApplication>
#include <QMessageBox>
#include <unistd.h>

UpdateWorker::UpdateWorker(QSettings *settings):
    settings(settings)
{
    qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QString downloadUrl = settings->value("RemoteAttentus/download",
                                          "http://54.164.73.251/webcmd/static/").toString();
    int build = settings->value("RemoteAttentus/versionbuild").toInt();
    QString version = settings->value("RemoteAttentus/versionmajor").toString() + "_" +
            settings->value("RemoteAttentus/versionminor").toString() + "_" +
            QVariant(build + 1).toString();
    updateFileName = "RemoteAttentus" + version + ".dbf";
    url = downloadUrl + updateFileName;
}

UpdateWorker::~UpdateWorker()
{
    timer->stop();
    delete timer;
    delete httpWorker;
}

void UpdateWorker::updateCheck()
{
    qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    httpWorker->downloadFile(url);
}

void UpdateWorker::initialize()
{
    qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    httpWorker = new HttpClient();

    connect(httpWorker, SIGNAL(downloadFinished()), SLOT(doUpdate()));

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateCheck()));
    int timeout = settings->value("RemoteAttentus/updatecheck", "1200000").toInt(); // default 20 min
    if(timeout < 30000) {
        timeout = 30000;
    }
    timer->start(timeout);
}

void UpdateWorker::doUpdate()
{
    qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;

    if(QFile(updateFileName).exists()) {
        QDir dir;
        dir.remove("RemoteAttentus.dbf");
        dir.rename(updateFileName, "RemoteAttentus.dbf");
#ifdef Q_OS_WIN32
        QStringList args = QCoreApplication::arguments();
        QString execCmd = "RemoteAttentusNew.exe";
        if(QFileInfo(args.first()).fileName().compare("RemoteAttentusNew.exe") == 0) {
            execCmd = "RemoteAttentus.exe";
            if(!dir.remove("RemoteAttentus.exe")) {
                //QMessageBox::information(NULL, "teste", "Erro ao remover " + QFileInfo(args.first()).fileName());
            }
            if(!dir.rename("RemoteAttentus.dbf", "RemoteAttentus.exe")) {
                //QMessageBox::information(NULL, "teste", "Erro ao renomear " + QFileInfo(args.first()).fileName());
            }
        } else {
            if(!dir.remove("RemoteAttentusNew.exe")) {
                //QMessageBox::information(NULL, "teste", "Erro ao remover " + QFileInfo(args.first()).fileName());
            }
            if(!dir.rename("RemoteAttentus.dbf", "RemoteAttentusNew.exe")) {
                //QMessageBox::information(NULL, "teste", "Erro ao renomear " + QFileInfo(args.first()).fileName());
            }
        }
#elif defined(Q_OS_LINUX)
        QProcess process;
        QString execCmd = "./RemoteAttentus";

        process.start("chmod +x RemoteAttentus.dbf");
        process.waitForFinished(-1); // will wait forever until finished

        dir.remove("RemoteAttentus.old");
        dir.rename("RemoteAttentus", "RemoteAttentus.old");
        dir.rename("RemoteAttentus.dbf", "RemoteAttentus");
#endif
        if(QFile::exists(execCmd)) {
            emit updating();
            timer->stop();
            QThread::sleep(1);
            execl(execCmd.toUtf8().data(), execCmd.toUtf8().data(), "rename", NULL);
        }
    }
}

