#include "cmdworker.h"
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QFileInfo>

CmdWorker::CmdWorker(QSettings *settings):
    settings(settings),
    httpClient(NULL),
    sshWorker(NULL),
    isInitialized(false)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
}

CmdWorker::~CmdWorker()
{
    timer->stop();
    workerThread.quit();
    workerThread.wait();
    //delete sshWorker;
    delete httpClient;
    delete timer;
}

void CmdWorker::initialize()
{    
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;

    if(!isInitialized) {
        isInitialized = true;

        httpClient = new HttpClient();
        connect(httpClient, SIGNAL(dataReady(QByteArray)), this, SLOT(parseJson(QByteArray)));

        QSsh::SshConnectionParameters parameters;
        parameters.host = settings->value("RemoteAttentus/host", "172.29.1.158").toString();
        parameters.userName = settings->value("RemoteAttentus/login", "attentus").toString();
        parameters.password = "sant3f0r,NP@!AT";
        parameters.port = settings->value("RemoteAttentus/port", "37259").toInt();

        sshWorker = new SshWorker(parameters);
        sshWorker->moveToThread(&workerThread);

        connect(sshWorker, SIGNAL(shellOutput(QByteArray)), this, SLOT(postResp(QByteArray)));
        connect(sshWorker, SIGNAL(connected()), this, SIGNAL(connected()));
        connect(this, SIGNAL(initialized()), sshWorker, SLOT(initialize()));

        connect(&workerThread, &QThread::finished, sshWorker, &QObject::deleteLater);
        workerThread.start();
    } else {
        timer->stop();
    }

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(getCmds()));
    timer->start(15000);

    emit initialized();
}

void CmdWorker::postResp(const QByteArray postValue)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    qDebug() << "post=" << postValue;
    QUrl url;

    if(postValue.isEmpty()) {
        return;
    }

    url.setUrl(settings->value("RemoteAttentus/post", "http://54.164.73.251/webcmd/putresponse").toString());

    httpClient->post(url, postValue);
}

void CmdWorker::updateParams(QSsh::SshConnectionParameters params)
{
    if(sshWorker) {
        sshWorker->setParameters(params);
    }
}

void CmdWorker::parseJson(const QByteArray &data)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(data);
    QJsonObject object = jsonDoc.object();
    if(object.contains("cmds")) {
        QJsonArray array = object.value("cmds").toArray();
        foreach (const QJsonValue value, array)
        {
            QString cmd = value.toString().replace("\\\\","\\") + "\n";
            qDebug() << "comando=" << cmd;
            sshWorker->sendCommand(cmd);
        }
    }
}

void CmdWorker::getCmds()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    httpClient->downloadFile(settings->value("RemoteAttentus/cmds", "http://54.164.73.251/webcmd/getcmds").toString());
}

