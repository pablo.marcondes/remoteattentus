#ifndef WEB2SRVWORKER_H
#define WEB2SRVWORKER_H

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QMutexLocker>
#include <QSettings>
#include <QTimer>

#include "httpclient.h"
#include "sftpclient.h"

class Web2SrvWorker : public QObject
{
    Q_OBJECT

public:
    Web2SrvWorker(QSettings *settings);
    ~Web2SrvWorker();

    void updateParams(QSsh::SshConnectionParameters params);

public slots:
    void initialize();

private slots:
    void getWeb2Srvs();
    void parseJson(const QByteArray &data );
    void handleFinishedDownload();
    void handleTransferFinished();

private:
    QSettings *settings;
    HttpClient *httpClient;
    HttpClient *httpDownloadClient;
    QMutex httpMutex;
    SftpClient *sftpClient;
    bool isInitialized;
    QTimer *timer;
    QString filePath;

signals:
    void initialized();
    void connected();
};

#endif // WEB2SRVWORKER_H
