/**************************************************************************
**
** This file is part of QSsh
**
** Copyright (c) 2012 LVK
**
** Contact: andres.pagliano@lvklabs.com
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
**************************************************************************/

#include "sftpclient.h"

#include <QtDebug>
#include <QFileInfo>
#include <QDir>

SftpClient::SftpClient(QObject *parent) :
    QObject(parent), m_recursive(false), m_overwriteMode(QSsh::SftpSkipExisting), m_port(22), m_transferMode(DOWNLOAD), m_connection(0)
{
}

SftpClient *SftpClient::localFile(const QString &localFile)
{
    QFileInfo info(localFile);
    m_localFilename = info.fileName();
    if(info.isDir()) {
        m_recursive = true;
    }

    return this;
}

SftpClient *SftpClient::remoteFile(const QString &remoteFile)
{
    m_remoteFilename = remoteFile;
    if(remoteFile.endsWith(QDir::separator())) {
        m_recursive = true;
    }

    return this;
}

SftpClient *SftpClient::host(const QString &host)
{
    this->m_host = host;

    return this;
}

SftpClient *SftpClient::port(const quint16 &port)
{
    this->m_port = port;

    return this;
}

SftpClient *SftpClient::user(const QString &user)
{
    this->m_user = user;

    return this;
}

SftpClient *SftpClient::pass(const QString &pass)
{
    this->m_pass = pass;

    return this;
}

SftpClient *SftpClient::recursive(const bool &recursive)
{
    this->m_recursive = recursive;

    return this;
}

SftpClient *SftpClient::overwrite(const bool &overwrite)
{
    this->m_overwriteMode = overwrite ? QSsh::SftpOverwriteExisting : QSsh::SftpSkipExisting;

    return this;
}

SftpClient *SftpClient::transferMode(const SftpClient::TransferMode &transferMode)
{
    this->m_transferMode = transferMode;

    return this;
}

bool SftpClient::isConfigured()
{
    return !(m_localFilename.isEmpty() || m_remoteFilename.isEmpty()
            || m_host.isEmpty() || m_user.isEmpty() || m_pass.isEmpty());
}

void SftpClient::upload()
{
    m_transferMode = UPLOAD;
    connectAndTransfer();
}

void SftpClient::download()
{
    m_transferMode = DOWNLOAD;
    connectAndTransfer();
}

void SftpClient::connectAndTransfer()
{
    QSsh::SshConnectionParameters params = createParams();

    m_connection = new QSsh::SshConnection(params, this); // TODO free this pointer!

    connect(m_connection, SIGNAL(connected()), SLOT(onConnected()));
    connect(m_connection, SIGNAL(error(QSsh::SshError)), SLOT(onConnectionError(QSsh::SshError)));

    qDebug() << "SecureUploader: Connecting to host" << m_host;

    m_connection->connectToHost();
}


QSsh::SshConnectionParameters SftpClient::createParams()
{
    QSsh::SshConnectionParameters params;
    params.host = m_host;
    params.userName = m_user;
    params.password = m_pass;
    params.authenticationType = QSsh::SshConnectionParameters::AuthenticationByPassword;
    params.timeout = 30;
    params.port = m_port;

    return params;
}

void SftpClient::onConnected()
{
    qDebug() << "SecureUploader: Connected";
    qDebug() << "SecureUploader: Creating SFTP channel...";

    m_channel = m_connection->createSftpChannel();

    if (m_channel) {
        connect(m_channel.data(), SIGNAL(initialized()),
                SLOT(onChannelInitialized()));
        connect(m_channel.data(), SIGNAL(initializationFailed(QString)),
                SLOT(onChannelError(QString)));
        connect(m_channel.data(), SIGNAL(finished(QSsh::SftpJobId, QString)),
                SLOT(onOpfinished(QSsh::SftpJobId, QString)));

        m_channel->initialize();

    } else {
        qDebug() << "SecureUploader: Error null channel";
    }
}

void SftpClient::onConnectionError(QSsh::SshError err)
{
    qDebug() << "SecureUploader: Connection error" << err;
}

void SftpClient::onChannelInitialized()
{
    qDebug() << "SecureUploader: Channel Initialized";

    QSsh::SftpJobId job;
    switch (m_transferMode) {
    case UPLOAD:
        job = m_channel->uploadFile(m_localFilename, m_remoteFilename, m_overwriteMode);
        break;
    case DOWNLOAD:
        job = m_channel->downloadFile(m_remoteFilename, m_localFilename, m_overwriteMode);
        break;
    default:
        break;
    }


    if (job != QSsh::SftpInvalidJob) {
        qDebug() << "SecureUploader: Starting job #" << job;
    } else {
        qDebug() << "SecureUploader: Invalid Job";
    }
}

void SftpClient::onChannelError(const QString &err)
{
    qDebug() << "SecureUploader: Error: " << err;
}

void SftpClient::onOpfinished(QSsh::SftpJobId job, const QString &err)
{
    qDebug() << "SecureUploader: Finished job #" << job << ":" << (err.isEmpty() ? "OK" : err);
    emit transferFinished();
}





