#ifndef HTTPWORKER_H
#define HTTPWORKER_H

#include <QWaitCondition>
#include <QMutex>
#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QSettings>
#include <QFile>
#include <QByteArray>

class HttpClient: public QObject
{
    Q_OBJECT

public:
    HttpClient();
    ~HttpClient();

    void downloadFile(QUrl url);
    void upload(QUrl url, QFile *file);
    void post(QUrl url, const QByteArray postValue);
    void startRequest(QUrl url);
    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);

private:
    QNetworkAccessManager *qnam;
    QNetworkReply *reply;
    QSettings *settings;
    QFile *file;
    QMutex httpMutex;

    void initialize();

private slots:
    void httpFinished();
    void httpReadyRead();
    void uploadFinished();

signals:
    void dataReady(const QByteArray data);
    void downloadFinished();
};

#endif // HTTPWORKER_H
