#include "sshworker.h"

#include <iostream>

SshWorker::SshWorker(QSsh::SshConnectionParameters &parameters):
    m_connection(NULL),
    parameters(parameters),
    m_shell(NULL),
    m_stdin(new QFile(this)),
    queueMutex(new QMutex()),
    keepRunning(false)
{
    //qDebug()() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
}

SshWorker::~SshWorker()
{
    keepRunning = false;
    if(m_connection) {
        m_connection->disconnectFromHost();
    }
    delete m_stdin;
    delete m_connection;
    delete timer;
    delete queueMutex;
}

void SshWorker::sendCommand(QString cmd)
{
    //qDebug()() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    cmdQueue.enqueue(cmd);
    hasMessage.wakeAll();
}

void SshWorker::startConnection()
{
    //qDebug()() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    if(!m_connection) {
        parameters.authenticationType = QSsh::SshConnectionParameters::AuthenticationByPassword;
        parameters.timeout = 30;

        m_connection = new QSsh::SshConnection(parameters);

        connect(m_connection, SIGNAL(connected()), SLOT(handleConnected()));
        connect(m_connection, SIGNAL(dataAvailable(QString)), SLOT(handleShellMessage(QString)));
        connect(m_connection, SIGNAL(error(QSsh::SshError)), SLOT(handleConnectionError(QSsh::SshError)));

        if (!m_stdin->open(stdin, QIODevice::ReadOnly | QIODevice::Unbuffered)) {
            std::cerr << "Error: Cannot read from standard input." << std::endl;
            return;
        }
    }

    m_connection->connectToHost();
}

void SshWorker::doDisconnect()
{
    m_connection->disconnectFromHost();
}

void SshWorker::handleConnected()
{
    //qDebug()() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    m_shell = m_connection->createRemoteShell();
    connect(m_shell.data(), SIGNAL(started()), SLOT(handleShellStarted()));
    connect(m_shell.data(), SIGNAL(readyReadStandardOutput()), SLOT(handleRemoteStdout()));
    connect(m_shell.data(), SIGNAL(readyReadStandardError()), SLOT(handleRemoteStderr()));
    connect(m_shell.data(), SIGNAL(closed(int)), SLOT(handleChannelClosed(int)));
    m_shell->start();

    emit connected();
}

void SshWorker::handleShellMessage(const QString str)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    emit shellOutput(str.toUtf8());
}

void SshWorker::handleConnectionError(QSsh::SshError error)
{
    emit connectionError(error);
    std::cerr << "Erro: " << error << std::endl;
}

void SshWorker::handleShellStarted()
{
    //QSocketNotifier * const notifier = new QSocketNotifier(0, QSocketNotifier::Read, this);
    //connect(notifier, SIGNAL(activated(int)), SLOT(handleStdin()));
}

void SshWorker::handleRemoteStdout()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QByteArray data = m_shell->readAllStandardOutput();
    if(data.length() <= 0) {
        return;
    }
    qDebug() << "stdout=" << data;
    emit shellOutput(data);
}

void SshWorker::handleRemoteStderr()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    emit shellOutput(m_shell->readAllStandardError().data());
}

void SshWorker::handleChannelClosed(int exitStatus)
{
    std::cerr << "Shell closed. Exit status was " << exitStatus << ", exit code was "
              << m_shell->exitCode() << "." << std::endl;
}

void SshWorker::handleStdin()
{
    m_shell->write(m_stdin->readLine());
}

void SshWorker::disable()
{
    keepRunning = false;
}

void SshWorker::initialize()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    startConnection();
    keepRunning = true;
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(send()));
    timer->start(5000);
}

void SshWorker::send()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;

    if(cmdQueue.isEmpty()) {
        return;
    }

    //queueMutex->lock();
    QString cmd = cmdQueue.dequeue();
    if( m_shell->write(cmd.toUtf8().data()) == -1){
        startConnection();
        m_shell->write(cmd.toUtf8().data());
    }
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__ << ":" << __LINE__ ;
    //queueMutex->unlock();
}

void SshWorker::setParameters(QSsh::SshConnectionParameters parameters)
{
    this->parameters = parameters;
}

