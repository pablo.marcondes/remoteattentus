#ifndef CMDWORKER_H
#define CMDWORKER_H

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QMutexLocker>
#include <QSettings>
#include <QTimer>

#include "httpclient.h"
#include "sshworker.h"
#include "sftpclient.h"

class CmdWorker : public QObject
{
    Q_OBJECT

public:
    CmdWorker(QSettings *settings);
    ~CmdWorker();

    void updateParams(QSsh::SshConnectionParameters params);

public slots:
    void initialize();

private slots:
    void getCmds();
    void parseJson(const QByteArray &data );
    void postResp(const QByteArray postValue);

private:
    QSettings *settings;
    HttpClient *httpClient;
    SshWorker *sshWorker;
    QTimer *timer;
    QThread workerThread;
    bool isInitialized;

signals:
    void initialized();
    void connected();
};

#endif // CMDWORKER_H
