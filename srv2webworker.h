#ifndef SRV2WEBWORKER_H
#define SRV2WEBWORKER_H

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QMutexLocker>
#include <QSettings>
#include <QTimer>

#include "httpclient.h"
#include "sftpclient.h"

class Srv2WebWorker : public QObject
{
    Q_OBJECT

public:
    Srv2WebWorker(QSettings *settings);
    ~Srv2WebWorker();

    void updateParams(QSsh::SshConnectionParameters params);

public slots:
    void initialize();

private slots:
    void getSrvs2Web();
    void parseJson(const QByteArray &data );
    void handleFinishedDownload();
    void handleTransferFinished();

private:
    QSettings *settings;
    HttpClient *httpClient;
    HttpClient *httpUploadClient;
    QMutex httpMutex;
    SftpClient *sftpClient;
    bool isInitialized;
    QTimer *timer;
    QString filePath;

signals:
    void initialized();
    void connected();
};

#endif // SRV2WEBWORKER_H
