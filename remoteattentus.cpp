#include "remoteattentus.h"
#include "ui_remoteattentus.h"

#include <iostream>
#include <unistd.h>
#include <qdebug.h>
#include <QListWidgetItem>
#include <QThread>
#include <QString>
#include <QSocketNotifier>
#include <QTimer>
#include <QDir>

RemoteAttentus::RemoteAttentus(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RemoteAttentus)
{    
    ui->setupUi(this);
    settings = new QSettings(QString("config.ini"), QSettings::IniFormat);
    flagConnected = false;

    settings->setValue("RemoteAttentus/versionmajor", 1);
    settings->setValue("RemoteAttentus/versionminor", 0);
    settings->setValue("RemoteAttentus/versionbuild", 27);
    QString version = settings->value("RemoteAttentus/versionmajor").toString() + "." +
            settings->value("RemoteAttentus/versionminor").toString() + "." +
            settings->value("RemoteAttentus/versionbuild").toString();
    ui->lbVersion->setText("Versão: " + version);

    parameters.host = settings->value("RemoteAttentus/host", "172.29.1.158").toString();
    parameters.userName = settings->value("RemoteAttentus/login", "attentus").toString();
    parameters.password = "sant3f0r,NP@!AT";
    parameters.port = settings->value("RemoteAttentus/port", "37259").toInt();


    ui->leIPAddress->setText(parameters.host);
    ui->leUserName->setText(parameters.userName);
    ui->lePassword->setText(parameters.password);
    ui->lePort->setText(QVariant(parameters.port).toString());

    connect(ui->leIPAddress, SIGNAL(editingFinished()), SLOT(updateParams()));
    connect(ui->leUserName, SIGNAL(editingFinished()), SLOT(updateParams()));
    connect(ui->lePassword, SIGNAL(editingFinished()), SLOT(updateParams()));
    connect(ui->lePort, SIGNAL(editingFinished()), SLOT(updateParams()));

    cmdWorker = new CmdWorker(settings);
    cmdWorker->moveToThread(&cmdWorkerThread);

    connect(cmdWorker, SIGNAL(connected()), SLOT(uiEnableConnected()));
    connect(&cmdWorkerThread, &QThread::finished, cmdWorker, &QObject::deleteLater);
    connect(this, SIGNAL(letDownload()), cmdWorker, SLOT(initialize()));

    cmdWorkerThread.start();

    web2SrvWorker = new Web2SrvWorker(settings);
    web2SrvWorker->moveToThread(&web2SrvWorkerThread);
    connect(&web2SrvWorkerThread, &QThread::finished, web2SrvWorker, &QObject::deleteLater);
    connect(this, SIGNAL(letDownload()), web2SrvWorker, SLOT(initialize()));

    web2SrvWorkerThread.start();

    srv2WebWorker = new Srv2WebWorker(settings);
    srv2WebWorker->moveToThread(&srv2WebWorkerThread);
    connect(&srv2WebWorkerThread, &QThread::finished, srv2WebWorker, &QObject::deleteLater);
    connect(this, SIGNAL(letDownload()), srv2WebWorker, SLOT(initialize()));

    srv2WebWorkerThread.start();

    updateWorker = new UpdateWorker(settings);
    updateWorker->moveToThread(&updateWorkerThread);
    connect(updateWorker, SIGNAL(updating()), SLOT(prepareToUpdate()));
    connect(&updateWorkerThread, &QThread::finished, updateWorker, &QObject::deleteLater);
    connect(this, SIGNAL(letStartUpdates()), updateWorker, SLOT(initialize()));

    updateWorkerThread.start();
    emit letStartUpdates();

    connect(ui->pbConnect, SIGNAL(clicked()), this, SLOT(connectClicked()));
    connect(ui->pbConnect, SIGNAL(clicked()), cmdWorker, SLOT(initialize()));
    connect(ui->pbDisconnect, SIGNAL(clicked()), this, SLOT(disconnectClicked()));

    connect(ui->pbDownload, SIGNAL(clicked()), cmdWorker, SLOT(initialize()));
    connect(ui->cbLoop, SIGNAL(clicked(bool)), this, SLOT(updateDownloads(bool)));

    emit letDownload();

    uiDisableConnected();
}

RemoteAttentus::~RemoteAttentus()
{
    cmdWorkerThread.terminate();
    updateWorkerThread.terminate();
    delete updateWorker;
    delete cmdWorker;
    delete web2SrvWorker;
    delete srv2WebWorker;
    delete settings;
    delete ui;
}

void RemoteAttentus::connectClicked()
{
    ui->pbConnect->setEnabled(false);
}

void RemoteAttentus::disconnectClicked()
{
    cmdWorkerThread.terminate();
    updateWorkerThread.terminate();
    uiDisableConnected();
}

void RemoteAttentus::updateDownloads(bool checked)
{
    if(!checked) {
        cmdWorkerThread.terminate();
    }
}

void RemoteAttentus::prepareToUpdate()
{
    cmdWorkerThread.terminate();
}

void RemoteAttentus::uiEnableConnected()
{
    ui->pbDisconnect->setEnabled(true);
    ui->pbConnect->setEnabled(false);
}

void RemoteAttentus::uiDisableConnected()
{
    ui->pbDisconnect->setEnabled(false);
    ui->pbConnect->setEnabled(true);
}

void RemoteAttentus::updateParams()
{
    parameters.host = ui->leIPAddress->text();
    parameters.userName = ui->leUserName->text();
    parameters.password = ui->lePassword->text();
    parameters.port = ui->lePort->text().toInt();

    cmdWorker->updateParams(parameters);
}
