#include "remoteattentus.h"
#include <stdio.h>
#include <unistd.h>
#include <QApplication>
#include <QtNetwork>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>

int main(int argc, char *argv[])
{    
    QApplication a(argc, argv);
    QNetworkProxyFactory::setUseSystemConfiguration(true);
    //QStringList args = QCoreApplication::arguments();
    //QMessageBox::information(NULL, "teste", QFileInfo(args.first()).fileName());
#ifdef Q_OS_WIN32
    if(a.arguments().contains("rename"))
    {
        QThread::sleep(2);
        QDir dir;
        dir.remove("RemoteAttentus.dbf");
        dir.remove("RemoteAttentus.old");
        dir.rename("RemoteAttentus.exe", "RemoteAttentus.old");
        QFile::copy(QFileInfo(a.arguments().first()).fileName(), "RemoteAttentus.exe");
        execl("RemoteAttentus.exe", "RemoteAttentus.exe", NULL);
        return 0;
    }
    else
#endif
    {
        RemoteAttentus w;
        w.show();

        return a.exec();
    }
}
