#ifndef UPDATEWORKER_H
#define UPDATEWORKER_H

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QMutexLocker>
#include <QSettings>
#include <QUrl>
#include <QTimer>

#include <httpclient.h>

class UpdateWorker : public QThread
{
    Q_OBJECT
public:
    UpdateWorker(QSettings *settings);
    ~UpdateWorker();


public slots:
    void initialize();

private:
    QString updateFileName;
    QSettings *settings;
    HttpClient *httpWorker;
    QTimer *timer;
    QUrl url;

private slots:
    void doUpdate();
    void updateCheck();

signals:
    void updating();
};

#endif // UPDATEWORKER_H
