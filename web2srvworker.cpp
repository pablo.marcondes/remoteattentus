#include "web2srvworker.h"
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QFileInfo>

Web2SrvWorker::Web2SrvWorker(QSettings *settings):
    settings(settings),
    httpClient(NULL),
    httpDownloadClient(NULL),
    sftpClient(NULL),
    isInitialized(false)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
}

Web2SrvWorker::~Web2SrvWorker()
{
    delete httpClient;
    delete httpDownloadClient;
    delete sftpClient;
}

void Web2SrvWorker::initialize()
{    
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;

    if(!isInitialized) {
        isInitialized = true;

        httpClient = new HttpClient();
        connect(httpClient, SIGNAL(dataReady(QByteArray)), this, SLOT(parseJson(QByteArray)));

        httpDownloadClient = new HttpClient();
        connect(httpDownloadClient, SIGNAL(downloadFinished()), this, SLOT(handleFinishedDownload()));

        sftpClient = new SftpClient();
        connect(sftpClient, SIGNAL(transferFinished()), this, SLOT(handleTransferFinished()));
    }

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(getWeb2Srvs()));
    timer->start(30000);

    emit initialized();
}

void Web2SrvWorker::handleFinishedDownload()
{
    sftpClient->host(settings->value("RemoteAttentus/host", "172.29.1.158").toString())->
            user(settings->value("RemoteAttentus/login", "attentus").toString())->
            pass("sant3f0r,NP@!AT")->
            port(settings->value("RemoteAttentus/port", "37259").toInt())->overwrite(true);
    QString fileName = QFileInfo(filePath).fileName();
    sftpClient->localFile(fileName)->remoteFile(fileName);

    sftpClient->upload();
}

void Web2SrvWorker::handleTransferFinished()
{
    qDebug() << "transferiu";
}

void Web2SrvWorker::parseJson(const QByteArray &data)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(data);
    QJsonObject object = jsonDoc.object();

    if(object.contains("web2srv")) {
        QJsonArray array = object.value("web2srv").toArray();
        foreach (const QJsonValue value, array)
        {
            filePath = value.toString().replace("\\\\","\\");
            qDebug() << "web2srv=" << filePath;
            QString fileUrl = settings->value("RemoteAttentus/download", "http://54.164.73.251/webcmd/static/").toString() + "upload/" + filePath;
            httpDownloadClient->downloadFile(fileUrl);
        }
    }
}

void Web2SrvWorker::getWeb2Srvs()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QString url = settings->value("RemoteAttentus/cmds", "http://54.164.73.251/webcmd/getcmds").toString()
            .replace("getcmds", "getweb2srv");
    httpClient->downloadFile(url);
}
