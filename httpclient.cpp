#include "httpclient.h"

#include <QFileInfo>
#include <QThread>
#include <QHttpMultiPart>
#include <iostream>
#include <qdebug.h>

HttpClient::HttpClient():
    qnam(NULL)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
}

HttpClient::~HttpClient()
{

}

void HttpClient::initialize()
{
    if(!qnam) {
        qnam = new QNetworkAccessManager();
    }
}

void HttpClient::downloadFile(QUrl url)
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    initialize();
    httpMutex.lock();
    QFileInfo fileInfo(url.path());
    QString fileName = fileInfo.fileName();

    if (QFile::exists(fileName)) {
        QFile::remove(fileName);
    }

    file = new QFile(fileName);
    if (!file->open(QIODevice::WriteOnly)) {
        qDebug("Erro ao salvar arquivo.");
        delete file;
        file = 0;
        qDebug("destravando erro ao salvar arquivo.");
        httpMutex.unlock();
        return;
    }

    // schedule the request
    startRequest(url);
}

void HttpClient::post(QUrl url, const QByteArray postValue)
{
    initialize();
    QByteArray postData;
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    QString postKey = "text";
    postData.append(postKey).append("=").append(postValue).append("&");

    qnam->post(request,postData);
}

void HttpClient::upload(QUrl url, QFile *file)
{
    initialize();

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart filePart;
    filePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"file\"; filename=\""+ file->fileName() + "\""));

    file->open(QIODevice::ReadOnly);
    filePart.setBodyDevice(file);
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart
    multiPart->append(filePart);

    QNetworkRequest request(url);

    QNetworkReply *reply = qnam->post(request, multiPart);
    multiPart->setParent(reply); // delete the multiPart with the reply
    connect(reply, SIGNAL(finished()), this, SLOT(uploadFinished()));
//    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT  (uploadError(QNetworkReply::NetworkError)));
//    connect(reply, SIGNAL(uploadProgress(qint64, qint64)), this, SLOT  (uploadProgress(qint64, qint64)));
}

void HttpClient::startRequest(QUrl url)
{
    reply = qnam->get(QNetworkRequest(url));
    connect(reply, SIGNAL(finished()),
            this, SLOT(httpFinished()));
    connect(reply, SIGNAL(readyRead()),
            this, SLOT(httpReadyRead()));
}

void HttpClient::httpFinished()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    file->flush();
    file->close();

    if (reply->error()) {
        file->remove();
        qDebug("Download falhou.");
    }

    reply->deleteLater();
    reply = 0;
    delete file;
    file = 0;
    std::cout << "liberando por fim de download" << std::endl;
    httpMutex.unlock();
    emit downloadFinished();
}

void HttpClient::httpReadyRead()
{
    //qDebug() << QThread::currentThreadId() << __FILE__ << ":" << __FUNCTION__;
    QByteArray data = reply->readAll();
    //qDebug() << data;
    if (file) {
        file->write(data);

        emit dataReady( data );
    }
}

void HttpClient::uploadFinished()
{

}

void HttpClient::sslErrors(QNetworkReply *, const QList<QSslError> &errors)
{
    QString errorString;
    foreach (const QSslError &error, errors) {
        if (!errorString.isEmpty())
            errorString += ", ";
        errorString += error.errorString();
    }

    QString errorMsg = tr("One or more SSL errors has occurred: %1").arg(errorString);
    //qDebug() << errorMsg;
    reply->ignoreSslErrors();
}
